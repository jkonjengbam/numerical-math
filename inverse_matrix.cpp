

void inverse_matrix(float A[10][10],float D[10][10],int order)
 {
     int i,j,k,l,m,n;
     float id[10][10],B[10][10],AA[10][10];
     n=order;
     //call inverse of A

	  for(l=1;l<n+1;l++)
  		 {
    		for(m=1;m<n+1;m++)
      		id[l][m]=0;
       }

	  for(l=1;l<n+1;l++)
   	 id[l][l]=1;

     for(l=1;l<n+1;l++)
    	{
     		for(m=1;m<n+1;m++)
      		AA[l][m]=A[l][m];
    	}

    for(l=(n+1);l<(2*n)+1;l++)
 	   {
   	  for(m=1;m<n+1;m++)
      	 AA[l-n][m+n]=id[l-n][m];
	    }

    for(k=1;k<(n+1);k++)
    	{
      	double dummy=AA[k][k];
	      for(j=(2*n);j>(k-1);j--)
   	     {AA[k][j]=AA[k][j]/dummy;}
      	for(i=1;i<(n+1);i++)
	        {
   		      if(i!=k)
         		 {
			         dummy=AA[i][k];
   			      for(j=(2*n);j>(k-1);j--)
      				    AA[i][j]=AA[i][j]-(dummy*(AA[k][j]));
		           }//if loop closed
		       }//end i loop
	      }//k loop ends

    	for(l=n+1;l<(2*n)+1;l++)
	     {
   		   for(m=1;m<n+1;m++)
		          B[l-n][m]=AA[l-n][m+n]; //inverse of a stored in b
	     }

  		for(l=1;l<(n+1);l++)
        {
          for(m=1;m<(n+1);m++)
          	D[l][m]=B[l][m];
        }//D is the final inverse of A matrix
 }



