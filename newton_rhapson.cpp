//implementation of the NR-Routine.
//Newton-Rhapson Technique (NR) for a single variable

#include<iostream.h>
#include<math.h>

//Add your own code here.
------------------------------------------------------------
float FX(float x)   //original function where F(X)=0 always
 {
  //////////////////(see the application: newton1.cpp)////////
 }
------------------------------------------------------------
 //Add your own code here.
float F_X(float x)   //derivative function
 {
  /////////////////(see the application: newton1.cpp)/////////
 }
-----------------------------------------------------------
//Define the main routine here
//This routine finds out the root or roots.

float NR1(float initial_x,double tol,int iter)
 {
  int n;
  float x_old,x_new,N,D;

  //Prototype declarations for functions that will be used later on
  float FX(float);
  float F_X(float);

  x_old=initial_x;//initiallising pricedure
  n=1;//count to 1
  while(n<=iter)
   {
    N=FX(x_old);
    D=F_X(x_old);
    if(N==0)
     {
      return(x_old);
	  }
    else if(D==0)
     {
      cout<<"\n NR not applicable to the given system ";
      return(0);
     }
    //all is well do the following procedure
    x_new=x_old-(N/D);//updating value of x
    double diff=x_new-x_old;
    //check if the difference is less enough to comply with
    //the set tolerance limit
    if(fabs(diff)<=tol)
     {
      return(x_new);
     }
    //else update x value
    else
     {
      x_old=x_new;
      n++;//increment count
     }
   }//while loop ends
   return(0);
 }