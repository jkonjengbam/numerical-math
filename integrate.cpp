


//<integrate.cpp>
//implementation of the integration routine
#include<iostream.h>
#include<iomanip.h>
#include<math.h>

void main()
 {
   float initial_x,final_x;
   int n;//similar to N of integrate() routine
   char ch;
   float integration;//this will correspond to the integrated value
   float integrate(float a,float b,int N);
   initial_x=0.0;
   final_x=1.0;
   do
    {
     	cout<<"\n Enter the value of n(the no.of sampling points) :";
   	cin>>n;
   	integration=integrate(initial_x,final_x,n);
   	cout<<"\n Integration of the function is :"<<integration;
      cout<<endl;
      cout<<"\n Do you wish to continue(y/n)?";
      cin>>ch;
    }while(ch=='y'||ch=='Y');
 }

//function define here.
float f(float x)
 {
  float expression=pow(x,2);
  return(expression);
 }

/*Routine : integrate()
	Integration of a function by Simpson's one-third rule
	I=Integral of f(x),between the limits
	x=a to x=b , with N sampling points
*/
//define the module as follows
float integrate(float a,float b,int N)
 {
  float h,x,s;
  int p;
  float f(float);
  if(N%2!=0)
    N++;  //make N even.
  h=(b-a)/N;
  x=a;
  s=f(x)+f(x+N*h);
  int i=1;//initialising
  again:if(i==N)
  			 {
           s=(h/3.0)*s;
           return(s);
          }
         else
          {
           s=s+(f(x+i*h)*p);
           i++;
           p=6-p;
           goto again;
          }
 }

