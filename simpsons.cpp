

//////////////////////////////////////////////////////////////////
//<simpsons.cpp>
/*Routine : integrate()
	Integration of a function by Simpson's one-third rule
	I=Integral of f(x),between the limits
	x=a to x=b , with N sampling points
*/

//define the module as follows
float integrate(float a,float b,int N)
 {
  float h,x,s;
  int p;
  float f(float);  //prototype of f(x);
  if(N%2!=0)
    N++;  //make N even.
  h=(b-a)/N;
  x=a;
  s=f(x)+f(x+N*h);
  int i=1;//initialising
  again:if(i==N)
  			 {
           s=(h/3.0)*s;
           return(s);
          }
         else
          {
           s=s+(f(x+i*h)*p);
           i++;
           p=6-p;
           goto again;
          }
 }

