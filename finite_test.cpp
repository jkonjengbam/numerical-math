//implementation of finite difference technique.
/* Description: Example 6.2 of S.K.Gupta("Numerical Methods for
		Engineers",page 265.).
*/
#include<iostream.h>
#include<iomanip.h>
#include<conio.h>
#include<math.h>
#include<process.h>

#define delta_x     0.2


//prototype declarations
void thomas(float a[],float b[],float c[],float d[],float [],int N);

void main()
 {
  //define the tridiagonal elements as below.
  int i,n,max_iteration;
  float a[10],b[10],c[10],f[10],old_y[10],new_y[10];
  //define the initial value of the roots as below.
  cout<<"\n The no. of grid points is denoted by n.";
  cout<<"\n Therefore, the no. of intervals is n-1";
  cout<<"\n Enter the value of n :";
  cin>>n;
  cout<<"\n Enter max_iteration :";
  cin>>max_iteration;
  for(i=1;i<(n+1);i++)
    old_y[i]=0.5;
  float z1=1/(6.0*pow(delta_x,2));
  float z2=1/(2.0*delta_x);

  for(i=1;i<(n+1);i++)
    {
      a[i]=z1+z2;
      b[i]=-(2.0*z1);
      c[i]=z1-z2;
    }
  a[1]=0.0;
  b[1]=-((2.0*z1)+(2.0/delta_x)+6.0);
  c[1]=2.0*z1;
  a[n]=2.0*z1;
  c[4]=0.0;


  cout<<"\n Print the tridiagonal elements."<<endl;
  cout<<"\n ------------------------------------------------------------"<<endl;
  cout<<setw(16)<<"a[]"<<setw(16)<<"b[]"<<setw(16)<<"c[]"<<endl;
  cout<<"\n ------------------------------------------------------------"<<endl;
  for(i=1;i<(n+1);i++)
    {
      cout<<setw(16)<<a[i]<<setw(16)<<b[i]<<setw(16)<<c[i]<<endl;
    }
  //Supply the constant matrix with that of the known values
  //of the initial roots of y.
  for(int iter=1;iter<max_iteration;iter++)
   {
     for(int k=1;k<(n+1);k++)
         f[k]=2.0*pow(old_y[k],2);
     f[1]=f[1]-6-(2.0/delta_x);
     //perform the thomas algorithm now.
     thomas(a,b,c,f,new_y,n);

     double diff=(double)(new_y[1]-old_y[1]);
     if(fabs(diff)<=0.001)
  		{
        float m=0.0;
   	  cout<<"\n Print answer."<<"   iteration ="<<iter<<endl;
        for(int i=1;i<(n+1);i++)
         {
            cout<<m<<"\t"<<new_y[i]<<endl;
            m=m+0.2;
         }
       cout<<"\n Exiting prog.,press a key.";
       getch();
       exit(1);
      }//endif
     //else update values of old_y[] by new_y[].
     for(i=1;i<(n+1);i++)
  		  old_y[i]=new_y[i];
   }//iter loop closes
  cout<<"\n Press any key to Quit program.";
  getch();
 }

 // Module : Thomas algorithm
/* Description : a[],b[],c[] are the diagonal matrix elements.
		x[] column matrix gives the solution roots corresponding
      to the above matrix set and a given constant matrix on the
      right hand side.
*/

void thomas(float a[],float b[],float c[],float d[],float x[],int N)
 {
	float cc[10],dd[10];//dimensions of new c and d
   float numerator;
   cc[1]=c[1]/b[1];
   dd[1]=d[1]/b[1];
   for (int k=1;k<N-1;k++)
   	{
      	cc[k+1]=c[k+1]/(b[k+1]-(a[k+1]*cc[k]));
         numerator=d[k+1]-(a[k+1]*dd[k]);
         dd[k+1]=numerator/(b[k+1]-(a[k+1]*cc[k]));
      }
   dd[N]=(d[N]-(a[N]*dd[N-1]))/(b[N]-(a[N]*cc[N-1]));
   //Do back sweeping to get the solutions or roots,x[]
   x[N]=dd[N];
   for (int k=N-1;k>0;k--)
   		x[k]=dd[k]-(cc[k]*x[k+1]);
 }


