// module 4
//Matrix multiplication of A and B matrices
void matrix_mult(float a[10][10],float b[10][10],float c[10][10],
								int m,int n,int l)
	{
   	for(int i=1;i<(m+1);i++)
       {
        for(int j=1;j<(l+1);j++)
         {
          float sum=0.0;
          for(int k=1;k<(n+1);k++)
           sum=sum+(a[i][k]*b[k][j]);
          c[i][j]=sum;
         }//j loop ends
        }//i loop ends
   }                                 