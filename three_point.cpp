//<three_point.cpp>

#include<iostream.h>
#include<iomanip.h>
#include<conio.h>
#include<assert.h>

struct data_point
 {
  float x;
  float y;
 }p[20];     //global variable.

void main()
 {
  float part1,part2,part3;
  int N;
  float selected_x;
  //prototypes.
  float L0_dash(float);
  float L1_dash(float);
  float L2_dash(float);

  cout<<"\n Enter the no. of data points. :";
  cin>>N;
  cout<<"\n Input the data points below."<<endl;
  cout<<"\n (use TAB key between the inputs for a data point.)";
  cout<<endl;
  cout<<endl;
  cout<<" X "<<"\t"<<" Y "<<endl;
  for(int i=0;i<N;i++)
   {
    cout<<endl;
    cin>>p[i].x;
    cout<<"\t";
    cin>>p[i].y;
   }
  cout<<endl;
  cout<<"\n Select x within the input data range :";
  cin>>selected_x;

  part1=p[0].y*L0_dash(selected_x);
  part2=p[1].y*L1_dash(selected_x);
  part3=p[2].y*L2_dash(selected_x);

  float sum=(part1+part2+part3);

  cout<<"\n Differentiated value at "<<selected_x<<" is : "<<sum;
  cout<<endl;
  cout<<"\n Quit on pressing a key.";
  getch();
 }

float L0_dash(float X)
 {
  float N=(2.0*X)-(p[1].x)-(p[2].x);
  float D=((p[0].x)-(p[1].x))*((p[0].x)-(p[2].x));
  assert(D!=0);
  float poly0=N/D;
  return(poly0);
 }

float L1_dash(float X)
 {
  float N=(2.0*X)-(p[0].x)-(p[2].x);
  float D=((p[1].x)-(p[0].x))*((p[1].x)-(p[2].x));
  assert(D!=0);
  float poly1=N/D;
  return(poly1);
 }

float L2_dash(float X)
 {
  float N=(2.0*X)-(p[0].x)-(p[1].x);
  float D=((p[2].x)-(p[0].x))*((p[2].x)-(p[1].x));
  assert(D!=0);
  float poly2=N/D;
  return(poly2);
 }
