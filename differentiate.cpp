//<differentiate.cpp>
//This is by far the most general and accurate algorithm.
//HAVE A GOOD TIME.
//-----------------------------------------------------------------
#include<iostream.h>
#include<iomanip.h>
#include<conio.h>
#include<math.h>

struct data
 {
  float x;
  float y;
 };

void main()
 {
  data p[10];
  int N;
  float base_x;
  float delta_x;
  float delta_y[100][100];
  //float delta1_y[5],delta2_y[5],delta3_y[5];
  float Y(float);//prototype.
  N=6;
  cout<<"\n No. of data points set is 6. ";
  //read the datas.
  cout<<"\n Enter the base x.:";
  cin>>base_x;
  cout<<"\n Select delta_x :";
  cin>>delta_x;
  /*Note: Delta_x can also be estimated if base_x and final_x
  				are known, alongwith N(the no. of data points.)
  */
  //Generate some datas from this base in the forward direction.
  p[0].x=base_x;
  for(int i=1;i<N;i++)
   {
    p[i].x=(p[i-1].x)+delta_x;
   }
  //calculate y(x);
  for(int i=0;i<N;i++)
   {
    p[i].y=Y(p[i].x);
   }
  cout<<endl;
  cout<<"\n DISPLAY THE DATA POINTS."<<endl;
  cout<<"\n X "<<"\t"<<" Y "<<endl;
  for(int i=0;i<N;i++)
   {
    cout<<"\n";
    cout<<p[i].x;
    cout<<"\t";
    cout<<p[i].y;
   }
  //initiate delta_y[][]
  int j=1;
  for(int i=0;i<(N-j);i++)
   {
     delta_y[i][j]=(p[i+1].y)-(p[i].y);
   }

  for(int j=2;j<N;j++)
   {
    for(int i=0;i<(N-j);i++)
     {
       delta_y[i][j]=(delta_y[i+1][j-1])-(delta_y[i][j-1]);
     }
   }
  float sum=0.0;
  for(int j=1;j<N;j++)
   {
    float invert_j=1.0/j;
    if((j%2)==0)  //if j is even,make it negative.
     {
      invert_j=-(invert_j);
     }
    sum=sum+(delta_y[0][j]*invert_j);
   }//j for loop closed 
  float y_dash=sum/delta_x;

  cout<<endl;
  cout<<"\n  The differentiated value at the selected base x"
  		<<"\n  is "<<y_dash;
  cout<<endl;
  cout<<"\n Quit on pressing a key.";
  getch();
 }

float Y(float X)
 {
  float term1,term2,term3,term4;
  term1=(float)pow(X,4);
  term2=(float)3.0*pow(X,3);
  term3=(float)6.0*pow(X,2);
  term4=2.0*X;
  float poly=term1-term2+term3-term4+5.0;
  return(poly);
 }
