#include "Matrix.h"

void main()
{
	// CMatrix( const int rowSize, const int colSize )
	CMatrix matrix1( 2, 2 );

	matrix1.setElement( 1.0, 0, 0 );
	matrix1.setElement( 2.0, 0, 1 );

	matrix1.setElement( 3.0, 1, 0 );
	matrix1.setElement( 4.0, 1, 1 );

	CMatrix matrix2 = matrix1;
	
	if ( matrix1 == matrix2 )
		cout <<"\n Successfully tested.." << endl;

	matrix2.reSize( 4, 4 );
	// Put extra elements here.
	matrix2.setElement( 5.0, 0, 2 );
	matrix2.setElement( 6.0, 0, 3 );

	matrix2.setElement( 7.0, 1, 2 );
	matrix2.setElement( 8.0, 1, 3 );

	matrix2.setElement( 9.0, 2, 0 );
	matrix2.setElement( 10.0 , 2, 1 );
	matrix2.setElement( 11.0, 2, 2 );
	matrix2.setElement( 12.0, 2, 3 );

	matrix2.setElement( 13.0, 3, 0 );
	matrix2.setElement( 14.0, 3, 1 );
	matrix2.setElement( 15.0, 3, 2 );
	matrix2.setElement( 16.0, 3, 3 );

	cout<< matrix2;

	CMatrix *inversedMatrix = matrix2.inverse();

/*	matrix2.reSize( 2, 2 );
	cout << endl << matrix2;

	matrix2.reSize( 5, 5 );
	cout << endl << matrix2;*/
}