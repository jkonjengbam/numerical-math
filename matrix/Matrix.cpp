// Matrix.cpp: implementation of the CMatrix class.
//
//////////////////////////////////////////////////////////////////////

#include "Matrix.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMatrix::CMatrix()
{
	m_rowSize = m_colSize = 0;
}

CMatrix::CMatrix( const int rowSize, const int colSize )
{
	m_rowSize = rowSize;
	m_colSize = colSize;

	int ndx1;

	// Allocate memory for the array
    m_array = new double*[m_rowSize];
    for (ndx1 = 0; ndx1 < m_rowSize; ndx1++)
		m_array[ndx1] = new double[m_colSize];

	Initialize();
}

const int CMatrix::Initialize( double val /*= 0.0*/ )
{
	int ndx1, ndx2;
	
	if ( m_array == 0 )
		return -1;

	// Initialze the array with given value
    for (ndx1 = 0; ndx1 < m_rowSize; ndx1++)
		for (ndx2 = 0; ndx2 < m_colSize; ndx2++)
             m_array[ndx1][ndx2] = val;

	return 0;
}

CMatrix::CMatrix( const CMatrix& newVal )
{
	m_rowSize = newVal.m_rowSize;
	m_colSize = newVal.m_colSize;

	int ndx1, ndx2;

	// Allocate memory for the array
    m_array = new double*[m_rowSize];
    for (ndx1 = 0; ndx1 < m_rowSize; ndx1++)
		m_array[ndx1] = new double[m_colSize];

	Initialize();

	// Copy array data to matrix
    for (ndx1 = 0; ndx1 < m_rowSize; ndx1++)
		for (ndx2 = 0; ndx2 < m_colSize; ndx2++)
             m_array[ndx1][ndx2] = newVal.m_array[ndx1][ndx2];
}

CMatrix::CMatrix( double **array, const int rowSize, const int colSize )
{
	m_rowSize = rowSize;
	m_colSize = colSize;

	int ndx1, ndx2;

	// Allocate memory for the array
    m_array = new double*[m_rowSize];
    for (ndx1 = 0; ndx1 < m_rowSize; ndx1++)
		m_array[ndx1] = new double[m_colSize];
	
	Initialize();

	// Copy array data to matrix
    for (ndx1 = 0; ndx1 < m_rowSize; ndx1++)
		for (ndx2 = 0; ndx2 < m_colSize; ndx2++)
             m_array[ndx1][ndx2] = array[ndx1][ndx2];
}

void CMatrix::getMatrixSize( int &rowSize, int &colSize ) const
{
	rowSize = m_rowSize;
	colSize = m_colSize;
}

const int CMatrix::getRowSize() const
{
	return( m_rowSize );
}

const int CMatrix::getColSize() const
{
	return( m_colSize );
}

void CMatrix::setElement( const double newVal, const int rowIndex, const int colIndex )
{
	m_array[rowIndex][colIndex] = newVal;
}

double CMatrix::getElement( const int rowIndex, const int colIndex ) const
{
	return( m_array[rowIndex][colIndex] );
}

void CMatrix::empty()
{
	Initialize();
}

const int CMatrix::setToDiagonalMatrix( double newVal )
{
	if ( !isSquare() )
		return( -1 );

	Initialize();

    for (int ndx1 = 0; ndx1 < m_rowSize; ndx1++)
		m_array[ndx1][ndx1] = newVal;

	return( 0 );
}

bool CMatrix::isIdentity() const
{
	int ndx1, ndx2;
	bool status = false;

	if ( m_rowSize != m_colSize )
		return( status );

	for (ndx1 = 0; ndx1 < m_rowSize; ndx1++)
	{
		for (ndx2 = 0; ndx2 < m_colSize; ndx2++)
		{
			if ( ndx1 == ndx2 )
			{
				// Check if diagonal elements are unity
				if ( m_array[ndx1][ndx2] == 1.0 ) 
				{
					status = true;
				}
			}
			else if ( m_array[ndx1][ndx2] == 0.0 ) 
			{
				status = true;
			}
			else
			{
				status = false;
				break;
			}
		}
	}

	return( status );
}

bool CMatrix::isSquare() const
{
	if ( m_rowSize == m_colSize )
		return( true );

	return( false );
}

bool CMatrix::isSameOrder( const CMatrix& rCMatrix ) const
{
	int rowSize, colSize;

	rCMatrix.getMatrixSize( rowSize, colSize );

	if ( ( m_rowSize == rowSize ) && ( m_colSize == colSize ) )
		return( true );

	return( false );
}

CMatrix& CMatrix::operator=( const CMatrix& rCMatrix )
{
	if ( this == &rCMatrix )
		return( *this );

	int ndx1, ndx2;
	int rowSize, colSize;

	rCMatrix.getMatrixSize( rowSize, colSize );

	// Redefine the size of array member
	if ( m_array )
	{
		// Delete the array
		for ( ndx1 = 0; ndx1 < m_rowSize; ndx1++)
			delete [] m_array[ndx1];
		delete [] m_array;	
	}

	m_rowSize = rowSize;
	m_colSize = colSize;

	// Allocate new sizes and initialize them.
    m_array = new double*[m_rowSize];
    for (ndx1 = 0; ndx1 < m_rowSize; ndx1++)
		m_array[ndx1] = new double[m_colSize];

	Initialize( 0.0 );

    for (ndx1 = 0; ndx1 < rowSize; ndx1++)
		for (ndx2 = 0; ndx2 < colSize; ndx2++)
             m_array[ndx1][ndx2] = rCMatrix.getElement( ndx1, ndx2 );

	return( *this );
}

bool CMatrix::operator==( const CMatrix& rCMatrix )
{
	int rowSize, colSize;
	int ndx1, ndx2;

	rCMatrix.getMatrixSize( rowSize, colSize );

	if ( rowSize != m_rowSize || colSize != m_colSize )
		return( false );
	// Proceed for every element validation
	for (ndx1 = 0; ndx1 < rowSize; ndx1++)
	{
		for (ndx2 = 0; ndx2 < colSize; ndx2++)
		{
			if ( m_array[ndx1][ndx2] != rCMatrix.getElement( ndx1, ndx2 ) )
				return( false );
		}
	}

	return( true);
}

bool CMatrix::operator!=( const CMatrix& rCMatrix )
{
	return( !operator ==( rCMatrix ) );
}

const int CMatrix::reSize( const int rowSize, const int colSize )
{
	if ( this == 0 )
		return -1;

	// Test next criteria
	if ( rowSize == m_rowSize && colSize == m_colSize )
		return -1; // re-sizing need not be done

	int ndx1, ndx2;

	// Put current matrix object somewhere..
	CMatrix *tempMatrix = this;
	// Add data to new matrix/ re-sized matrix
	double **tmpArr = CMatrix::createArray( rowSize, colSize );
	
	// Copy matrix data to tmpArr1
	int tmpRowLen, tmpColLen;
	tmpRowLen = ( m_rowSize < rowSize )? m_rowSize: rowSize;
	tmpColLen = ( m_colSize < colSize )? m_colSize: colSize;

    for (ndx1 = 0; ndx1 < tmpRowLen; ndx1++)
		for (ndx2 = 0; ndx2 < tmpColLen; ndx2++)
             tmpArr[ndx1][ndx2] = this->getElement( ndx1, ndx2 );
	
	// Update the matrix after re-sizing
	CMatrix matrix( tmpArr, rowSize, colSize );
	(*this) = matrix;

	// Set tmp to null
	tempMatrix = 0;

	CMatrix::freeArray( tmpArr, rowSize );

	return 0;
}

CMatrix& CMatrix::arrayToMatrix( double **array, const int rowSize, const int colSize )
{
	CMatrix *matrix = new CMatrix( array, rowSize, colSize );
	return( *matrix );
}

double** CMatrix::matrixToArray( CMatrix& matrix, int &rowSize, int &colSize ) 
{
	matrix.getMatrixSize( rowSize, colSize );

	double **tmpArr = (double**) CMatrix::createArray( rowSize, colSize );

	int ndx1, ndx2;

	// Copy matrix data to array
    for (ndx1 = 0; ndx1 < rowSize; ndx1++)
		for (ndx2 = 0; ndx2 < colSize; ndx2++)
             tmpArr[ndx1][ndx2] = matrix.getElement( ndx1, ndx2 );

	return( tmpArr );
}

CMatrix* CMatrix::inverse()
{
	if ( !isSquare() )
		return( 0 );

	// Proceed further if square matrix

	// Create an identity matrix of the same order
	CMatrix *identityMatrix = new CMatrix( m_rowSize, m_colSize );
	identityMatrix->setToDiagonalMatrix( 1.0 );

#ifdef _DEBUG
	cout << (*identityMatrix);
#endif

	// Create an augmented matrix 
	// ( size of current matrix + identity matrix )
	int ndx1, ndx2, ndx3;
	int augmentedColSize;

	augmentedColSize = 2 * m_colSize;

	CMatrix augmentedMatrix = (*this);
	augmentedMatrix.reSize( m_rowSize, augmentedColSize );

	// Append identity matrix data 
    for (ndx1 = 0; ndx1 < m_rowSize; ndx1++)
	{
		for (ndx2 = m_colSize; ndx2 < augmentedColSize; ndx2++)
		{
            double tmpValue = identityMatrix->getElement( ndx1, ndx2 - m_colSize ); 
			augmentedMatrix.setElement( tmpValue, ndx1, ndx2 );
		}
	}

#ifdef _DEBUG
		cout <<"\n Augmented matrix.." << endl;
		cout << augmentedMatrix;
#endif

	// Delete the identityMatrix now as not required any longer
	delete identityMatrix;

	// Define a temporary variable
	double dummy;

	for (ndx1 = 0; ndx1 < augmentedMatrix.getRowSize(); ndx1++ )
	{
		double pivotElement = augmentedMatrix.getElement( ndx1, ndx1 ); // pivot element
		dummy = pivotElement;

		for (ndx2 = (augmentedColSize - 1); ndx2 >= ndx1; ndx2-- )
		{
			double augmentedMatrixData = augmentedMatrix.getElement( ndx1, ndx2 );
			augmentedMatrixData = augmentedMatrixData / dummy;

			augmentedMatrix.setElement( augmentedMatrixData, ndx1, ndx2 );
		}

		for (ndx3 = 0; ndx3 < augmentedMatrix.getRowSize(); ndx3++ )
		{
			if ( ndx3 != ndx1 )
			{
				dummy = augmentedMatrix.getElement( ndx3, ndx1 );

				for (ndx2 = ( augmentedColSize - 1 ); ndx2 >= ndx1; ndx2-- )
				{
					double tmpValue1 = augmentedMatrix.getElement( ndx3, ndx2 );
					double tmpValue2 = augmentedMatrix.getElement( ndx1, ndx2 );

					double augmentedMatrixData = tmpValue1 - ( dummy * tmpValue2 );
					augmentedMatrix.setElement( augmentedMatrixData, ndx3, ndx2 );
				}
			}
		}
	}

#ifdef _DEBUG
	cout << "\n Augmented matrix after processing..." << endl;
	cout << augmentedMatrix << endl;
#endif

	// Inverse matrix
	CMatrix *inverseMatrix = new CMatrix( m_rowSize, m_colSize );
	for (ndx1 = 0; ndx1 < m_rowSize; ndx1++ )
	{
		for (ndx2 = 0; ndx2 < m_colSize; ndx2++ )
		{
			//
			double tmpValue = augmentedMatrix.getElement( ndx1, ndx2 + 4 );
			inverseMatrix->setElement( tmpValue, ndx1, ndx2 );
		}
	}	

	cout << "\nInverse of the matrix..." << endl;
	cout << (*inverseMatrix) << endl;
	return( inverseMatrix );
}

CMatrix* CMatrix::transpose()
{
	int rowSize, colSize;
	int ndx1, ndx2;

	rowSize = colSize = 0;

	if ( this == 0 )
		return 0;
	if ( m_rowSize < 1 || m_colSize < 1 )
		return 0;

	double **tmpArr = CMatrix::matrixToArray( (*this), rowSize, colSize );
	// Store transposed matrix here
	double **newArr = CMatrix::createArray( colSize, rowSize );
	
	// Perform transposition now
	for (ndx1 = 0; ndx1 < rowSize; ndx1++)
		for (ndx2 = 0; ndx2 < colSize; ndx2++)
             newArr[ndx2][ndx1] = tmpArr[ndx1][ndx2];

	CMatrix *tmpMatrix = new CMatrix( newArr, colSize, rowSize );
	
	CMatrix::freeArray( tmpArr, rowSize );
	CMatrix::freeArray( newArr, colSize );

	return( tmpMatrix );
}

CMatrix::~CMatrix()
{
	// Delete the array
    for ( int ndx1 = 0; ndx1 < m_rowSize; ndx1++)
		delete [] m_array[ndx1];
	delete [] m_array;
}

double** CMatrix::createArray( const int rowSize, const int colSize )
{
	int ndx1, ndx2;

	double **array;
	// Allocate memory for the array
    array = new double*[rowSize];
    for (ndx1 = 0; ndx1 < rowSize; ndx1++)
		array[ndx1] = new double[colSize];
	
	// Initialize array
    for (ndx1 = 0; ndx1 < rowSize; ndx1++)
		for (ndx2 = 0; ndx2 < colSize; ndx2++)
             array[ndx1][ndx2] = 0.0;

	return( array );
}

void CMatrix::freeArray( double **array, const int rowSize )
{
	if ( array )
	{
		// Delete the array
		for ( int ndx1 = 0; ndx1 < rowSize; ndx1++)
			delete [] array[ndx1];
		delete [] array;
	}

	array = 0;
}

CMatrix& operator+( const CMatrix& rCMatrix1, const CMatrix& rCMatrix2 )
{
	int r1, c1, r2, c2;
	CMatrix *matrix = 0; // Initialize to null

	rCMatrix1.getMatrixSize( r1, c1 );
	rCMatrix2.getMatrixSize( r2, c2 );

	// Checking order consistency
	if ( r1 != r2 || c1 != c2 )
		return( *matrix );

	int ndx1, ndx2;
	int rowSize, colSize;

	rowSize = r1;
	colSize = c1;

	matrix = new CMatrix( rowSize, colSize );

	// Perform addition of matrix elements
	for (ndx1 = 0; ndx1 < rowSize; ndx1++)
		for (ndx2 = 0; ndx2 < colSize; ndx2++)
             matrix->m_array[ndx1][ndx2] = rCMatrix1.m_array[ndx1][ndx2] + rCMatrix2.m_array[ndx1][ndx2];

	return( *matrix );
}

CMatrix& operator-( const CMatrix& rCMatrix1, const CMatrix& rCMatrix2 )
{
	int r1, c1, r2, c2;
	CMatrix *matrix = 0; // Initialize to null

	rCMatrix1.getMatrixSize( r1, c1 );
	rCMatrix2.getMatrixSize( r2, c2 );

	if ( r1 != r2 || c1 != c2 )
		return( *matrix );

	int ndx1, ndx2;
	int rowSize, colSize;

	rowSize = r1;
	colSize = c1;

	matrix = new CMatrix( rowSize, colSize );

	// Perform addition of matrix elements
	for (ndx1 = 0; ndx1 < rowSize; ndx1++)
		for (ndx2 = 0; ndx2 < colSize; ndx2++)
             matrix->m_array[ndx1][ndx2] = rCMatrix1.m_array[ndx1][ndx2] - rCMatrix2.m_array[ndx1][ndx2];

	return( *matrix );
}

CMatrix& operator*( const CMatrix& rCMatrix, const double scalarProductVal )
{
	CMatrix *matrix = 0; // Initialize to null

	int rowSize, colSize;
	int ndx1, ndx2;

	rCMatrix.getMatrixSize( rowSize, colSize );

	matrix = new CMatrix( rowSize, colSize );

	// Perform addition of matrix elements
	for (ndx1 = 0; ndx1 < rowSize; ndx1++)
		for (ndx2 = 0; ndx2 < colSize; ndx2++)
             matrix->m_array[ndx1][ndx2] = rCMatrix.m_array[ndx1][ndx2] * scalarProductVal;

	return( *matrix );
}

CMatrix& operator*( const double scalarProductVal, const CMatrix& rCMatrix )
{
	CMatrix *matrix = 0; // Initialize to null

	int rowSize, colSize;
	int ndx1, ndx2;

	rCMatrix.getMatrixSize( rowSize, colSize );

	matrix = new CMatrix( rowSize, colSize );

	// Perform addition of matrix elements
	for (ndx1 = 0; ndx1 < rowSize; ndx1++)
		for (ndx2 = 0; ndx2 < colSize; ndx2++)
             matrix->m_array[ndx1][ndx2] = rCMatrix.m_array[ndx1][ndx2] * scalarProductVal;

	return( *matrix );
}

CMatrix& operator*( const CMatrix& rCMatrix1, const CMatrix& rCMatrix2 )
{
	int r1, c1, r2, c2;
	CMatrix *matrix = 0; // Initialize to null

	rCMatrix1.getMatrixSize( r1, c1 );
	rCMatrix2.getMatrixSize( r2, c2 );

	if ( c1 != r2 )
		return( *matrix ); // matrices not conformant

	int ndx1, ndx2, ndx3;
	int rowSize, colSize;

	rowSize = r1;
	colSize = c2;

	matrix = new CMatrix( rowSize, colSize );

	for (ndx1 = 0; ndx1 < r1; ndx1++)
	{
		for (ndx2 = 0; ndx2 < c2; ndx2++)
		{
			double sum = 0.0;

			for (ndx3 = 0; ndx3 < r2; ndx3++)
			{
				sum = sum + rCMatrix1.m_array[ndx1][ndx3] * rCMatrix2.m_array[ndx3][ndx2];
			}

			matrix->m_array[ndx1][ndx2] = sum;
		}
	}

	return( *matrix );
}

ostream& operator<<( ostream &out, const CMatrix& rCMatrix )
{
	int ndx1, ndx2;

	int rowSize, colSize;
	rCMatrix.getMatrixSize( rowSize, colSize );

	out << "\nDisplaying matrix..\n";
    for (ndx1 = 0; ndx1 < rowSize; ndx1++)
	{
		for (ndx2 = 0; ndx2 < colSize; ndx2++)
		{
			out << rCMatrix.getElement( ndx1, ndx2 ) << "\t";
		}

		out << endl;
	}

	return out;
}