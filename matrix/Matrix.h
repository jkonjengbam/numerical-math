// Matrix.h: interface for the CMatrix class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MATRIX_H__61E887C3_27C4_11D5_B9B1_F8F700A7A77B__INCLUDED_)
#define AFX_MATRIX_H__61E887C3_27C4_11D5_B9B1_F8F700A7A77B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <iostream.h> 

//#define _declspec( dllexport ) DllExport

class /*DllExport*/ CMatrix  
{
public:
	// Constructor(s) and Destructors
	CMatrix( const int rowSize, const int colSize );
	CMatrix( double **array, const int rowSize, const int colSize );

	// Copy Constructor
	CMatrix( const CMatrix& newVal );

	virtual ~CMatrix();

	// Operator overloading
	// Assignment and Equality testing
	CMatrix& operator=( const CMatrix& rCMatrix );
	bool operator==( const CMatrix& rCMatrix );
	bool operator!=( const CMatrix& rCMatrix );

	// Global / Friend functions for other operations
	// Output to stream
	friend ostream& operator<<( ostream &out, const CMatrix& rCMatrix );
	// Other math operations
	friend CMatrix& operator+( const CMatrix& rCMatrix1, const CMatrix& rCMatrix2 );
	friend CMatrix& operator-( const CMatrix& rCMatrix1, const CMatrix& rCMatrix2 );
	friend CMatrix& operator*( const CMatrix& rCMatrix1, const CMatrix& rCMatrix2 );
	friend CMatrix& operator*( const CMatrix& rCMatrix1, const double scalarProductVal );
	friend CMatrix& operator*( const double scalarProductVal, const CMatrix& rCMatrix );

	// Public member functions
	// Size Operations -- reading
	void getMatrixSize( int &rowSize, int &colSize ) const;
	const int getRowSize() const;
	const int getColSize() const;
	// Re-sizing
	const int reSize( const int rowSize, const int colSize );

	// Element modifiers and accessing
	void setElement( const double newVal, const int rowIndex, const int colIndex );
	double getElement( const int rowIndex, const int colIndex ) const;

	// Other functions
	void empty(); // clears the matrix and sets elements to zero
	const int setToDiagonalMatrix( double newVal = 0.0 );
	// Validation
	bool isIdentity() const;
	bool isSquare() const;
	bool isSameOrder( const CMatrix& rCMatrix ) const;
	
	CMatrix* inverse(); // Find inverse	
	CMatrix* transpose(); // Find transpose
	
	// Global static functions
	// Conversions
	static CMatrix& arrayToMatrix( double **array, const int rowSize, const int colSize );
	static double** matrixToArray( CMatrix& matrix, int &rowSize, int &colSize );

private:
	// Private member data
	int m_rowSize;
	int m_colSize;
	double **m_array;

	// Private constructor
	CMatrix();

	//Private member functions
	const int Initialize( double val = 0.0 );

	static double** createArray( const int rowSize, const int colSize );
	static void freeArray( double **array, const int rowSize );

};

#endif // !defined(AFX_MATRIX_H__61E887C3_27C4_11D5_B9B1_F8F700A7A77B__INCLUDED_)
