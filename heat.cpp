//<heat.cpp>
//implementation of the shooting technique
//Finds use in solving heat-transfer problems,
//ODE-BVPs, as d2T/dt2=f(T).

#include<iostream.h>
#include<iomanip.h>
#include<conio.h>

//constant values and global variables.
#define h_prime    0.01 //heat transfer coefficient
#define ta        20.00 //ambient temperature
#define target   200.00 //value of T2(final value temperature)
#define L         10    //Length of the rod
#define h          2    //step size

const int size=(L/h)+1;


//prototype declaration of the function to be used in the main.
void shoot(float initial_z,float T1,float x[],float T[]);

void main()
 {
  float z0,distance[20],temperatures[20];
  char ch;
  do
  {
  	cout<<"\n Enter initial z value for shooting. :";
  	cin>>z0;
  	cout<<endl;
  	shoot(z0,40.0,distance,temperatures);
  	//Retrieve the values of distances and temperatures.
  	cout<<setw(15)<<"DISTANCES"<<setw(15)<<"TEMPERATURES"<<endl;
  	cout<<endl;
  	for(int i=0;i<size;i++)
    {
     cout<<setw(15)<<distance[i]<<setw(15)<<temperatures[i]<<endl;
    }
   cout<<"\n Wish to shoot again with a different z0(y/n)?";
   cin>>ch;
   }while(ch=='y'||ch=='Y');
  cout<<"\n Quit on pressing a key.";
  getch();
 }




//define the functions f1 and f2 here below as follows
float f1(float y)
  {
   float poly1=h_prime*(y-ta);
   return(poly1);
  }

float f2(float zz)
 {
 	float poly2=zz;
   return(poly2);
 }
//define the main module as shoot()
//x represents the distance at any instance from one end
//of the rod.
//T represents the final temperature at a particular x.

void shoot(float initial_z,float T1,float x[],float T[])
 {
 	float k1,k2,k3,k4;
   float sum1,sum2;
   float z[10];

   //Prototype declarations of the functions f1 and f2
	float f1(float);
	float f2(float);

   T[0]=T1;//initialising temperature to the given temperature T1
   z[0]=initial_z;

   for(int j=0;j<size;j++)
   	x[j]=j*h;
   for(int i=0;i<size;i++)
     {
     	k1=f1(T[i]);
      k2=f1(T[i]+(0.5*h*k1));
      k3=f1(T[i]+(0.5*h*k2));
		k4=f1(T[i]+h*k3);

		sum1=(k1+2.0*k2+2.0*k3+k4)*h;
      z[i+1]=z[i]+(sum1/6.0);

      k1=f2(z[i+1]);
      k2=f2(z[i+1]+(0.5*h*k1));
      k3=f2(z[i+1]+(0.5*h*k2));
		k4=f2(z[i+1]+h*k3);

      sum2=(k1+2.0*k2+2.0*k3+k4)*h;
      T[i+1]=T[i]+(sum2/6.0);

     }//for loop ends
 }

