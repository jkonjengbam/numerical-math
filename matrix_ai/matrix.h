#ifndef __MATRIX_H
# define __MATRIX_H

# include <vector>
# include <algorithm>
# include <cstddef>
# include <stdexcept>
# include <iomanip>
# include <limits>

using namespace std;

template <typename T>
class CMatrix
{
public:
    CMatrix(size_t rows, size_t cols)
      : rows_(rows), cols_(cols), data_(rows * cols)
    {
        std::fill(data_.begin(), data_.end(), (T)0);
    }

    CMatrix(size_t rows, size_t cols, const std::vector<T>& data)
      : rows_(rows), cols_(cols), data_(data)
    {
        if (rows * cols != data.size())
        {
            throw std::invalid_argument("invalid data size");
        }
    }

    CMatrix(const CMatrix<T>& other)
      : rows_(other.rows_), cols_(other.cols_), data_(other.data_) {}


    T& operator()(size_t row, size_t col)
    {
        return data_[row * cols_ + col];
    }

    const T& operator()(size_t row, size_t col) const
    {
        return data_[row * cols_ + col];
    }

    CMatrix<T>* operator+(const CMatrix<T>& other)
    {
        if (rows_ != other.rows_ || cols_ != other.cols_)
        {
            throw std::invalid_argument("matrices must have the same size");
        }

        CMatrix<T>* result = new CMatrix<T>(rows_, cols_);
        for (size_t i = 0; i < rows_; i++)
        {
            for (size_t j = 0; j < cols_; j++)
            {
                (*result)(i, j) = (*this)(i, j) + other(i, j);
            }
        }
        return result;
    }

    CMatrix<T>* operator-(const CMatrix<T>& other)
    {
        if (rows_ != other.rows_ || cols_ != other.cols_)
        {
            throw std::invalid_argument("matrices must have the same size");
        }

        CMatrix<T>* result = new CMatrix<T>(rows_, cols_);
        for (size_t i = 0; i < rows_; i++)
        {
            for (size_t j = 0; j < cols_; j++)
            {
                (*result)(i, j) = (*this)(i, j) - other(i, j);
            }
        }
        return result;
    }

    CMatrix<T>* operator*(const CMatrix<T>& other)
    {
        if (cols_ != other.rows_)
        {
            throw std::invalid_argument("invalid matrix size for multiplication");
        }

        CMatrix<T>* result = new CMatrix<T>(rows_, other.cols_);
        for (size_t i = 0; i < result->rows_; i++)
        {
            for (size_t j = 0; j < result->cols_; j++)
            {
                T sum = 0;
                for (size_t k = 0; k < cols_; k++)
                {
                    sum += (*this)(i, k) * other(k, j);
                }
                (*result)(i, j) = sum;
            }
        }
        return result;
    }

    CMatrix<T>* operator*(T scalar)
    {
        CMatrix<T>* result = new CMatrix<T>(rows_, cols_);
        for (size_t i = 0; i < rows_; i++)
        {
            for (size_t j = 0; j < cols_; j++)
            {
                (*result)(i, j) = (*this)(i, j) * scalar;
            }
        }
        return result;
    }

    T determinant() const
    {
        if (rows_ != cols_)
        {
            throw std::invalid_argument("matrix must be square");
        }

        CMatrix<T> A(*this);
        T det = 1;
        for (std::size_t i = 0; i < A.rows_; i++)
        {
            // Find pivot row
            std::size_t pivot = i;
            for (std::size_t j = i + 1; j < A.rows_; j++)
            {
                if (std::abs(A(j, i)) > std::abs(A(pivot, i)))
                {
                    pivot = j;
                }
            }

            // Swap rows
            A.swap_rows(i, pivot);

            // Update determinant
            if (i != pivot)
            {
                det *= -1;
            }

            // Normalize pivot row
            T pivot_val = A(i, i);
            if (pivot_val != 0)
            {
                A.scale_row(i, 1 / pivot_val);
            }

            // Eliminate pivot column
            for (std::size_t j = i + 1; j < A.rows_; j++)
            {
                A.add_multiple_of_row(j, i, -A(j, i));
            }
        }

        // Calculate determinant
        for (std::size_t i = 0; i < A.rows_; i++)
        {
            det *= A(i, i);
        }
        return det;
    }

    CMatrix<T>* inverse()
    {
        if (rows_ != cols_)
        {
            throw std::invalid_argument("matrix must be square");
        }

        CMatrix<T>* result = new CMatrix<T>(rows_, cols_);
        for (size_t i = 0; i < rows_; i++)
        {
            (*result)(i, i) = 1;
        }

        // Forward elimination
        for (size_t i = 0; i < rows_; i++)
        {
            // Find pivot element
            size_t pivot = i;
            for (size_t j = i + 1; j < rows_; j++)
            {
                if (std::abs((*this)(j, i)) > std::abs((*this)(pivot, i)))
                {
                    pivot = j;
                }
            }

            // Swap rows
            this->swap_rows(i,  pivot);
            result->swap_rows(i,  pivot);


            // Eliminate pivot element
            T pivot_value = (*this)(i, i);
            if (pivot_value == 0)
            {
                throw std::runtime_error("matrix is singular");
            }

            for (size_t j = 0; j < cols_; j++)
            {
                (*this)(i, j) /= pivot_value;
                (*result)(i, j) /= pivot_value;
            }

            for (size_t j = i + 1; j < rows_; j++)
            {
                T factor = (*this)(j, i);
                for (size_t k = 0; k < cols_; k++)
                {
                    (*this)(j, k) -= factor * (*this)(i, k);
                    (*result)(j, k) -= factor * (*result)(i, k);
                }
            }
        }

        // Backward substitution
        for (int i = rows_ - 1; i >= 0; i--)
        {
            for (int j = i - 1; j >= 0; j--)
            {
                T factor = (*this)(j, i);
                for (size_t k = 0; k < cols_; k++)
                {
                    (*this)(j, k) -= factor * (*this)(i, k);
                    (*result)(j, k) -= factor * (*result)(i, k);
                }
            }
        }

        return result;
    }

    CMatrix<T>* transpose()
    {
        CMatrix<T>* result = new CMatrix<T>(cols_, rows_);
        for (size_t i = 0; i < rows_; i++)
        {
            for (size_t j = 0; j < cols_; j++)
            {
                (*result)(j, i) = (*this)(i, j);
            }
        }
        return result;
    }

    friend std::ostream& operator<<(std::ostream& out, const CMatrix<T>& m)
    {
        out.precision(std::numeric_limits< double >::max_digits10);
        for (size_t i = 0; i < m.rows_; i++)
        {
            for (size_t j = 0; j < m.cols_; j++)
            {
                out << m(i, j) << "\t";
            }
            out << std::endl;
        }
        return out;
    }

    size_t rows() const { return rows_; }
    size_t cols() const { return cols_; }

protected:
    void swap_rows(std::size_t i, std::size_t j)
    {
        if (i >= rows_ || j >= rows_)
        {
            throw std::out_of_range("row index out of range");
        }

        std::swap_ranges(
            data_.begin() + i * cols_,
            data_.begin() + (i + 1) * cols_,
            data_.begin() + j * cols_);
    }

    void scale_row(std::size_t i, T s)
    {
        if (i >= rows_)
        {
            throw std::out_of_range("row index out of range");
        }

        for (std::size_t j = 0; j < cols_; j++)
        {
            (*this)(i, j) *= s;
        }
    }

    void add_multiple_of_row(std::size_t i, std::size_t j, T s)
    {
        if (i >= rows_ || j >= rows_)
        {
            throw std::out_of_range("row index out of range");
        }

        for (std::size_t k = 0; k < cols_; k++)
        {
            (*this)(i, k) += s * (*this)(j, k);
        }
    }

private:
    size_t rows_;
    size_t cols_;
    std::vector<T> data_;
};

#endif                                                      // __MATRIX_H
