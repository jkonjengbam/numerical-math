//Module 2 (successful)
//Runge Kutta -- two variables, or simply RK-4
//Here by default, only 2 process variables are selected,
//y1 and y2
//The two equations involving the two process variables are defined first
#include<iostream.h>
#include<iomanip.h>
#include<conio.h>
#include<process.h>

//prototype declaration.
void RK42(float h,float y10,float y20,int last);

void main()
 {
  RK42(0.02,2.0,1.0,10);
  cout<<"\n Press any key to quit.";
  getch();
 }




//define f1 and f2 as follows (default settings are given below)

float f1(float Y1)
 {
   float temp=(-100.0*Y1);
   return(temp);
 }//first function ends here

float f2(float Y1,float Y2)
 {
    float poly=(2.0*Y1)-Y2;
    return(poly);
 }//second function ends here

//Define RK-4 as follows
void RK42(float h,float y10,float y20,int last)
 {
 	char ch;
   float t;
   float y1[10],y2[10];
	float k1[2],k2[2],k3[2],k4[2];

   //Prototype declarations of the functions involved
   float f1(float);
   float f2(float,float);

   cout<<"\n Warning:Have you changed the functions as per your needs?"
       <<"\n , if not -- then do it first by going to the source file :"
       <<"\n C:\BC5\BIN\runge2.cpp";
   cout<<endl;
   cout<<"\n Do you wish to make changes in the functions(y/n):";
   cin>>ch;
   if(ch=='y')
   	{
      	cout<<"\n Exiting to alter functions ";
         exit(0);
      }

	y1[0]=y10;//initialising the arrays
	y2[0]=y20;

	for(int n=0;n<last;n++)
		{
   		k1[0]=f1(y1[n]);
      	k1[1]=f2(y1[n],y2[n]);

      	k2[0]=f1(y1[n]+(h/2.0)*k1[0]);
      	k2[1]=f2((y1[n]+(h/2.0)*k1[0]),(y2[n]+(h/2.0)*k1[1]));

      	k3[0]=f1(y1[n]+(h/2.0)*k2[0]);
      	k3[1]=f2((y1[n]+(h/2.0)*k2[0]),(y2[n]+(h/2.0)*k2[1]));

      	k4[0]=f1(y1[n]+h*k3[0]);
      	k4[1]=f2((y1[n]+h*k3[0]),(y2[n]+h*k3[1]));

      //calculate y1[] and y2[]
      	y1[n+1]=y1[n]+((h/6.0)*(k1[0]+2.0*k2[0]+2.0*k3[0]+k4[0]));
      	y2[n+1]=y2[n]+((h/6.0)*(k1[1]+2.0*k2[1]+2.0*k3[1]+k4[1]));

   	}//for loop ends here

 	//displaying the roots
  	cout<<"\n DISPLAY CALCULATED DATA. "<<endl;
   cout<<endl;
  	cout<<setw(15)<<"y1[] values"<<setw(15)<<"y2[] values"<<setw(15)<<"t"<<endl;
  	cout<<"---------------------------------------------------"<<endl;
   t=0.0;
  	for(int n=0;n<last;n++)
	  {
       cout<<setw(15)<<y1[n]<<setw(15)<<y2[n]<<setw(15)<<t<<endl;
       t=t+h;
     }
 }
