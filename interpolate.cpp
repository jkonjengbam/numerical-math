//Module 8
//Lagrangian's interpolation of function approximation
//May be use in curve fitting also
#include<iostream.h>
#include<iomanip.h>

#define max 30
//define now the main module called interpolate

//The routine shall return a number of type float
//In this routine , each point will be used to construct a function,
//and for any input x(a variable of function f)not necessarily one
//of the same data point ,corresponding f(x)
//which is reasonably true shall be return.

float interpolate(float x[max],float y[max],int N,float _x)
	{
    float lag,prod,_y;
/*    cout<<"\n Enter the number of data points(<30) :";
    cin>>N;
    cout<<"\n Enter the data points below :"<<endl;
    cout<<"----------------------------------"<<endl;
    cout<<" x      y           "<<endl;
    for(int i=0;i<N;i++)
       {cin>>x[i];cout<<"\t";cin>>y[i];cout<<endl;}

    cout<<"\n Enter the value of x(_x) for which f(x) is to"
    	  <<"\n be found out : ";
    cin>>_x; */
    lag=0.0;//initialising
    for(int j=0;j<N;j++)
     {
      prod=1.0;
       for(int i=0;i<N;i++)
        {
         if(i==j)
           continue;
         prod=prod*(_x-x[i])/(x[j]-x[i]);
        }//end i loop

      lag=lag+prod*y[j];
     }//end j loop
     _y=lag;
     return(_y);
   }
