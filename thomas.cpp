// Module 1
//Thomas algorithm, applicable in solving finite differece problems

void thomas_algo(float a[],float b[],float c[],float d[],float x[],int N)
 {
	float cc[50],dd[50];//dimensions of new c and d
   float numerator;
   cc[1]=c[1]/b[1];
   dd[1]=d[1]/b[1];
   for (int k=1;k<N-1;k++)
   	{
      	cc[k+1]=c[k+1]/(b[k+1]-(a[k+1]*cc[k]));
         numerator=d[k+1]-(a[k+1]*dd[k]);
         dd[k+1]=numerator/(b[k+1]-(a[k+1]*cc[k]));
      }
   dd[N]=(d[N]-(a[N]*dd[N-1]))/(b[N]-(a[N]*cc[N-1]));
   //Do back sweeping to get the solutions or roots,x[]
   x[N]=dd[N];
   for (int k=N-1;k>0;k--)
   		x[k]=dd[k]-(cc[k]*x[k+1]);
 }

