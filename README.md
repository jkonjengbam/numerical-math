# Numerical Math
The project here is based on the book "Numerical Methods for Engineers" by S.K. Gupta. I implemented the algorithms as part of fulfillment for Bachelors in Engineering (Chem) way back in 1998 and around. I hope some of the students find the sample codes useful.

## Numerical Algorithms 
This project implements several algorithms for solving linear (and non-linear) systems of equations, including the Newton-Raphson, ODM, and Thomas algorithms, etc. In addition, a generic matrix design and abstraction is provided to facilitate the implementation and use of these algorithms.

## Background
Linear systems of equations are a common type of mathematical problem that arise in many scientific and engineering applications. They have the form Ax = b, where A is a matrix of coefficients, x is a vector of unknowns, and b is a known vector of constants. There are several methods for solving linear systems, each with its own strengths and limitations. The Newton-Raphson method is a root-finding algorithm that can be used to solve systems of nonlinear equations, while the ODM and Thomas algorithms are efficient methods for solving tridiagonal systems of equations.

A nonlinear system is a system of equations or a mathematical model in which the relationships between the variables are not linear. In other words, the variables are not related by simple linear equations of the form y = a + bx, where a and b are constants.

Nonlinear systems are common in many fields, including physics, engineering, biology, economics, and computer science. They can be challenging to solve because they do not have a general closed-form solution, meaning that the solution cannot be expressed in terms of a finite number of elementary functions. Instead, the solution must be approximated using numerical methods, such as the Newton-Raphson method or the bisection method.

Nonlinear systems can exhibit complex behavior, such as multiple solutions, bifurcations, and chaos, which makes them interesting and challenging to study. They are often used to model real-world systems that exhibit nonlinear behavior, such as oscillators, biological populations, and electrical circuits.

## Algorithms
### Newton-Raphson
The Newton-Raphson algorithm is an iterative method for solving systems of nonlinear equations. It is based on the idea of linearizing the system around an initial guess for the solution, and then iteratively refining this guess until it converges to the true solution. The algorithm consists of the following steps:

Choose an initial guess for the solution, x0.
Compute the Jacobian matrix, J, of the system at x0.
Solve the linear system J(x1 - x0) = f(x0) to obtain x1.
Set x0 = x1 and repeat from step 2 until convergence.
The algorithm converges quadratically, meaning that the error decreases by a factor of the square of the number of iterations. However, it requires the computation of the Jacobian matrix at each iteration, which can be expensive for large systems.

### ODM
The ODM (orthogonal decomposition method) is an efficient algorithm for solving tridiagonal systems of equations. It decomposes the system into a series of independent subproblems that can be solved in parallel, making it well-suited for use on multicore processors. The algorithm consists of the following steps:

Decompose the system into a series of independent subproblems.
Solve each subproblem in parallel using the Thomas algorithm.
Combine the solutions to obtain the final solution to the original system.
The ODM has a complexity of O(n), making it much faster than other methods for tridiagonal systems, such as the Thomas algorithm.

### Thomas
The Thomas algorithm is a direct method for solving tridiagonal systems of equations. It takes advantage of the special structure of the coefficient matrix to reduce the complexity of the problem from O(n^3) to O(n). The algorithm consists of the following steps:

Transform the original system into a form that is more amenable to solution.
Solve the transformed system using forward and backward substitution.
The Thomas algorithm is relatively fast, but it requires the computation of intermediate values that may be expensive for large systems.

## Lagrange interpolation
Lagrange interpolation is a method for constructing a polynomial of degree n that passes through n + 1 data points (x0, y0), (x1, y1), ..., (xn, yn). The polynomial is given by the formula:

p(x) = y0L0(x) + y1L1(x) + ... + ynLn(x)

where Li(x) is the ith Lagrange basis polynomial, given by the formula:

Li(x) = (x - x0) * (x - x1) * ... * (x - xi-1) * (x - xi+1) * ... * (x - xn) / ((xi - x0) * (xi - x1) * ... * (xi - xi-1) * (xi - xi+1) * ... * (xi - xn))

The Lagrange basis polynomials are constructed such that they are zero at all points xj for j ≠ i, and 1 at xi. This ensures that the polynomial p(x) passes through the data points (xi, yi).

The process of constructing the polynomial p(x) using Lagrange interpolation is known as Lagrange interpolation. It is a simple and efficient method for constructing a polynomial that fits a given set of data points, but it is prone to oscillation (i.e., overfitting) when the degree of the polynomial is high.

Lagrange interpolation is often used in numerical analysis to approximate functions that are known only at a finite number of points. It is also used in other fields, such as computer graphics, to interpolate between known data points to generate smooth curves or surfaces.

## Runge-Kutta 4(2)
The Runge-Kutta 4(2) method (also known as the RK42 method) is a numerical method for solving ordinary differential equations (ODEs). It is a fourth-order method, meaning that it uses four evaluations of the ODE per step to achieve a higher accuracy than lower-order methods.

The RK42 method is a two-stage method, which means that it uses two intermediate estimates of the solution to compute the final solution at each time step. It can be described as follows:

Given the solution at time t, y(t), and the step size h, compute the intermediate estimates y1 and y2 using the formulas:
y1 = y(t) + h * f(t, y(t))
y2 = y(t) + h * (f(t + h/2, y1) + f(t, y(t))) / 2

Compute the solution at time t + h using the formula:
y(t + h) = y(t) + h * (f(t, y(t)) + 2f(t + h/2, y1) + f(t + h, y2)) / 6

The RK42 method is a popular choice for solving ODEs because it is relatively easy to implement and has good accuracy and stability properties. However, it is more computationally expensive than lower-order methods, such as the Euler method.

## Least squares
Least squares is a method for finding the line of best fit for a set of data points by minimizing the sum of the squares of the differences between the observed values and the values predicted by the model.

In other words, given a set of data points (x1, y1), (x2, y2), ..., (xn, yn), the least squares method finds the parameters a and b that minimize the sum of the squares of the differences (y - y')^2, where y' is the predicted value of y given by the model y' = a + bx.

The least squares method can be applied to linear or nonlinear models, but it is most commonly used for linear models of the form y = a + bx. The least squares method has several attractive properties, including unbiasedness, efficiency, and consistency, which make it a popular choice for estimating model parameters.

The least squares method can be computed using a variety of techniques, including the normal equations, singular value decomposition, and gradient descent. It is often used in regression analysis to fit a model to a set of data and make predictions about the response variable based on the predictor variables. It is also used in other fields, such as signal processing and image analysis, to fit models to data and extract useful information.

## Shooting technique
The shooting technique is a method for solving boundary value problems (BVPs) by reducing them to a system of initial value problems (IVPs). A BVP is a type of differential equation that specifies the values of the solution at two or more points (the "boundary" points), and the goal is to find a solution that satisfies these boundary conditions.

The shooting technique works by guessing a value for one of the unknown parameters in the problem (the "shooting parameter"), and then solving the resulting IVP using a standard numerical method, such as the Runge-Kutta method. The solution to the IVP is then compared to the boundary condition at the other boundary point, and the shooting parameter is adjusted based on the discrepancy between the two. This process is repeated until the discrepancy is small enough, at which point the solution to the BVP is considered to be found.

The shooting technique is a simple and effective method for solving BVPs, but it can be computationally expensive, especially for problems with many shooting parameters. It is also sensitive to the choice of initial guess for the shooting parameter, and it may not converge to the correct solution if the initial guess is too far from the true value. Despite these limitations, the shooting technique is widely used in various fields, including engineering, physics, and finance, to solve BVPs that arise in practice.

## Three-point method
The three-point algorithm is a method for estimating the derivative of a function at a given point using only three function evaluations. It is based on the idea of constructing a polynomial that fits the three data points and then taking the derivative of this polynomial at the desired point.

The three-point algorithm can be described as follows:

Given a function f(x) and a point x0 at which to estimate the derivative, choose two points x1 and x2 such that x1 < x0 < x2.
Evaluate the function at the three points: y1 = f(x1), y2 = f(x2), and y0 = f(x0).
Construct the polynomial p(x) that fits the three points using the formula:
p(x) = y0 * (x - x1) * (x - x2) / ((x0 - x1) * (x0 - x2)) + y1 * (x - x0) * (x - x2) / ((x1 - x0) * (x1 - x2)) + y2 * (x - x0) * (x - x1) / ((x2 - x0) * (x2 - x1))

Take the derivative of p(x) at x0 to obtain the estimate of the derivative:
p'(x0) = (y1 - y0) / (x1 - x0) + (y2 - y0) / (x2 - x0)

The three-point algorithm is a simple and efficient method for estimating the derivative of a function, but it is less accurate than higher-order methods that use more function evaluations. It is often used in numerical analysis and scientific computing to approximate the derivative of a function when it is not possible or practical to compute the derivative analytically.

## Matrix Design and Abstraction
To facilitate the implementation and use of these algorithms, a generic matrix design and abstraction is provided. The matrix design defines the structure and operations of matrices, including addition, multiplication, and inversion. The abstraction layer allows different matrix implementations to be used interchangeably, allowing the user to choose the most appropriate implementation for their needs.

In the project folder, you can see two variants of matrix abstraction and design. One is a legacy implementation and the other is newly developed using openAI. Surprisingly, it was super easy to develop the C++ implementation using AI and have the same tested through MathLAB and other tools available online.

# Working with this git Project

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/jkonjengbam/numerical-math.git
git branch -M main
git push -uf origin main
```

## License
GNU Public


