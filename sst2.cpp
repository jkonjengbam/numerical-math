
//<sst2.cpp>
/* Description : multivariable sst.(two variables,y2 and y3).
		Example problem (S.K.Gupta.,pp 76).
      Note that the sst is not the best technique here.
*/
#include<iostream.h>
#include<conio.h>
#include<iomanip.h>
#include<math.h>

void main()
 {
  int max_iteration;
  long double initial_y2,initial_y3;
  long double y2_new,y2_old,y3_new,y3_old;
  long double f1(long double,long double);//prototype
  long double f2(long double,long double);//prototype

  initial_y2=0.6;
  initial_y3=0.4;

  max_iteration=500;

  y2_old=initial_y2;
  y3_old=initial_y3;
  int count=0;
  do
   {
      count++;
  		y2_new=y2_old+((1.0/20)*f1(y2_old,y3_old));
      y3_new=y3_old+((1.0/24)*f2(y2_old,y3_old));
  		long double error1=y2_new-y2_old;

  		if(fabs(error1)<=0.001)
   		{
    			cout<<"\n PRINT THE ROOT."<<endl;
            float y2=(float)y2_new;
            float y3=(float)y3_new;
    			cout<<"\n y2 root = "<<y2
            	 <<"\n y3 root = "<<y3;
    			break;
   		}
  		//update y2_old,y3_old values.
  		y2_old=y2_new;
      y3_old=y3_new;
  	}while(count<max_iteration);
  cout<<"\n Quit on pressing a key.";
  getch();
 }

//function to be employed is explicitly defined here.
long double f1(long double y2,long double y3)
 {
  long double expression1=(-2.0*powl(y2,3))-(8.0*y2)+(4.0*y3)+4.0;
  return(expression1);
 }

long double f2(long double y2,long double y3)
 {
  long double expression2=(-4.0*y2)+powl(y3,2)+(3.0*y3)+1.0;
  return(expression2);
 }


