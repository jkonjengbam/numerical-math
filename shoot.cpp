//Module 3
//implementation of the shooting technique
//Finds use in solving heat-transfer problems,
//ODE-BVPs, as d2T/dt2=f(T).


//constant values
#define h_prime    0.01 //heat transfer coefficient
#define ta        20.00 //ambient temperature
#define target   200.00 //value of T2(final value temperature)

//Prototype declarations of the functions f1 and f2
float f1(float);
float f2(float);

//define the functions f1 and f2 here below as follows
float f1(float y)
  {
   float poly1=h_prime*(y-ta);
   return(poly1);
  }

float f2(float zz)
 {
 	float poly2=zz;
   return(poly2);
 }
//define the main module as shoot()
void shoot(float h,float initial_z,float T1,float L,float x[],float T[])
 {
 	float k1,k2,k3,k4;
   float sum1,sum2;
   float z[10];
   T[0]=T1;//initialising temperature to the given temperature T1
   z[0]=initial_z;
   int size=(L/h)+1;
   for(int j=0;j<size;j++)
   	x[j]=j*h;
   for(int i=0;i<size-1;i++)
     {
     	k1=f1(T[i]);
      k2=f1(T[i]+(0.5*h*k1));
      k3=f1(T[i]+(0.5*h*k2));
		k4=f1(T[i]+h*k3);

		sum1=(k1+2.0*k2+2.0*k3+k4)*h;
      z[i+1]=z[i]+(sum1/6.0);

      k1=f2(z[i]);
      k2=f2(z[i]+(0.5*h*k1));
      k3=f2(z[i]+(0.5*h*k2));
		k4=f2(z[i]+h*k3);

      sum2=(k1+2.0*k2+2.0*k3+k4)*h;
      T[i+1]=T[i]+(sum2/6.0);

     }//for loop ends
 }

