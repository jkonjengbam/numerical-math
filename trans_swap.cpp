//Module 6
//Transpose of a square matrix , transpose of A=B.
void matrix_transpose(float a[10][10],float b[10][10],int order)
 {
  int i,j;
  int n=order;
  for(i=0;i<n;i++)
   {
    for(j=0;j<n;j++)
     b[i][j]=a[j][i];
   }
 }

//Swapping two values a and b of type float
void swapf(float a,float b)
 {
  float temp;
  temp=a;
  a=b;
  b=temp;
 }

//Swapping two values a and b of type int
void swapi(int a,int b)
 {
  int temp;
  temp=a;
  a=b;
  b=temp;
 }