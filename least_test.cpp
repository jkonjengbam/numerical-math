//(successful)
//implementation of the least squares
#include<iostream.h>
#include<conio.h>
#include<iomanip.h>
#include<assert.h>

 //Least squares module
/* Description: Filename: least_test.cpp
		This file shall contain the routine  that will operate
      on a given set of data points.Here N=n+1 shall repre-
      sent the number of data points.
*/



struct points
	{
      float x;
      float y;
   }data[20];

void main(void)
 {
   int N;
   float slope,intercept;
   float sumx,sumxx,sumxy,sumy;
   cout<<"\n Enter the number of data points :";
   cin>>N;
   cout<<"\n Enter the data below."<<endl;
   for(int i=1;i<(N+1);i++)
   	{
        cout<<"\n Read data point "<<i<<" :"<<endl;
        cout<<"\n Enter for x :";cin>>data[i].x;
        cout<<"\n Enter for y :";cin>>data[i].y;
        cout<<"\n -------------------------------------";
      }
   sumx=0.0;
   sumxx=0.0;
   sumxy=0.0;
   sumy=0.0;
   for(int i=1;i<(N+1);i++)
   	{
			sumx+=data[i].x;
         sumxx+=(data[i].x)*(data[i].x);
         sumxy+=(data[i].x)*(data[i].y);
         sumy+=data[i].y;
      }
	float part1=N*sumxy;
   float part2=sumx*sumy;
   float part3=N*sumxx;
   float part4=sumx*sumx;
   float part5=part3-part4;

   assert(part5!=0);
   //calculate the slope of the line
   slope=(part1-part2)/part5;
	cout<<"\n slope="<<slope;
   float part6=-((slope*sumx)-sumy);
   //calculate the intecept
   intercept=part6/N;
   cout<<"\n intercept="<<intercept;
   cout<<"\n PRESS ANY KEY TO QUIT.";
   getch();
 }
