//<dy_upon_dx.cpp>
#include<iostream.h>
#include<iomanip.h>
#include<conio.h>
#include<math.h>

struct data
 {
  float x;
  float y;
 };

void main()
 {
  data p[10];
  int N;
  float base_x;
  float delta_x;
  float delta1_y[5],delta2_y[5],delta3_y[5];
  float Y(float);//prototype.
  N=4;
  //read the datas.
  cout<<"\n Enter the base x.:";
  cin>>base_x;
  cout<<"\n Select delta_x :";
  cin>>delta_x;
  //Generate some datas from this base in the forward direction.
  p[0].x=base_x;
  for(int i=1;i<N;i++)
   {
    p[i].x=(p[i-1].x)+delta_x;
   }
  //calculate y(x);
  for(int i=0;i<N;i++)
   {
    p[i].y=Y(p[i].x);
   }
  cout<<endl;
  cout<<"\n DISPLAY THE DATA POINTS."<<endl;
  cout<<"\n X "<<"\t"<<" Y "<<endl;
  for(int i=0;i<N;i++)
   {
    cout<<"\n";
    cout<<p[i].x;
    cout<<"\t";
    cout<<p[i].y;
   }
  delta1_y[0]=(p[1].y)-(p[0].y);
  delta1_y[1]=(p[2].y)-(p[1].y);
  delta1_y[2]=(p[3].y)-(p[2].y);

  delta2_y[0]=delta1_y[1]-delta1_y[0];
  delta2_y[1]=delta1_y[2]-delta1_y[1];

  delta3_y[0]=delta2_y[1]-delta2_y[0];

  float sum=delta1_y[0]-((1.0/2)*delta2_y[0])+((1.0/3)*delta3_y[0]);
  float y_dash=sum/delta_x;

  cout<<endl;
  cout<<"\n  The differentiated value at the selected base x"
  		<<"\n  is "<<y_dash;
  cout<<endl;
  cout<<"\n Quit on pressing a key.";
  getch();
 }

float Y(float X)
 {
  float term1,term2,term3,term4;
  term1=(float)pow(X,4);
  term2=(float)3.0*pow(X,3);
  term3=(float)6.0*pow(X,2);
  term4=2.0*X;
  float poly=term1-term2+term3-term4+5.0;
  return(poly);
 }
