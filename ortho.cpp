//(successful)
//Implementation of the ortogonal collocation
//example problem on S.K.Gupta
//USING THE VARIOUS ROUTINES AVAILABLE


#include<iostream.h>
#include<math.h>
#include<iomanip.h>
#include<conio.h>
#include"read_matrix.cpp"
#include"inverse_matrix.cpp"
#include"matrix_mult.cpp"

#define max_iteration  50

void main()
 {
  int n=4;
  int iter;
  float C[10][10],F[10][10],A[10][10],D[10][10];
  float y10,y20,y30,y40,C1,C2;
  float y1[50],y2[50],y3[50],y4[50];

  //define matrix A and B here below.
  cout<<"\n Enter elements of A matrix below."<<endl;
  read_matrix(A,n,n);
  //read initial values here below
  cout<<"\n Enter y10 :";cin>>y10;
  cout<<"\n Enter y20 :";cin>>y20;
  cout<<"\n Enter y30 :";cin>>y30;
  cout<<"\n Enter y40 :";cin>>y40;

  C1=A[2][2];
  C2=A[3][3];

  y1[1]=y10;
  y2[1]=y20;
  y3[1]=y30;
  y4[1]=y40;

  for(iter=1;iter<max_iteration;iter++)
  	{
     A[2][2]=C1-4.0*y2[iter];
     A[3][3]=C2-4.0*y3[iter];

     //set constant matrix
     F[1][1]=-6.0;
     F[2][1]=-2.0*pow(y2[iter],2);
     F[3][1]=-2.0*pow(y3[iter],2);
     F[4][1]=0.0;

     //call inverse of A

		inverse_matrix(A,D,n);

      //multiply D and F
      matrix_mult(D,F,C,4,4,1);

      y1[iter+1]=C[1][1];
      y2[iter+1]=C[2][1];
      y3[iter+1]=C[3][1];
      y4[iter+1]=C[4][1];

      float error=y1[iter+1]-y1[iter];
      if(error<=0.001)
      	{
          	cout<<endl;
            cout<<"\n THE ROOTS ARE GIVEN BELOW :"<<endl;
            cout<<"y1= "<<y1[iter+1]<<endl;
            cout<<"y2= "<<y2[iter+1]<<endl;
            cout<<"y3= "<<y3[iter+1]<<endl;
            cout<<"y4= "<<y4[iter+1]<<endl;
            break;
          }
      else
      	continue;
  		  }//for loop closed
      cout<<"\n Press any key to quit program.";
      getch();
 }

