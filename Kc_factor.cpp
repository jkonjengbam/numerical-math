//Temporary file
#include<dos.h>
#include<iostream.h>
#include<conio.h>
#include<process.h>

#include"interpolate.cpp"

#define n 8
/*Storing datas for the Kc-factor and the API gravity against
    variation (T1-T2).
	 The Kc-factors are stored in x50_array
	 The API gravity values are stored in y50_array
*/
float x50_array[n]={0.1,0.3,0.5,0.8,1.2,1.5,1.7,2.0};
float y50_array[n]={43,26.0,20,14.8,10,8.0,6.0,5.0};

float x100_array[n]={0.1,0.3,0.5,0.8,1.2,1.5,1.7,2.0};
float y100_array[n]={49.5,31.5,24,18,13.5,10.89,9,8.0};

float x200_array[n]={0.1,0.3,0.5,0.8,1.2,1.5,1.7,2.0};
float y200_array[n]={58.5,37.6,29,22,16,13,11.3,9.0};

float *x_array,*y_array;  //global variables.

void main()
 {
  float input_x,extrapolated_y;
  char ans;
  int delta_T;

  cout<<endl<<"Specify delta_T (T1-T2): ";
  cin>>delta_T;
  switch(delta_T)
   {
    case 50 : x_array=&(x50_array);
    			  y_array=&(y50_array);
              break;
    case 100: x_array=&(x100_array);
    			  y_array=&(y100_array);
              break;
    case 200: x_array=&(x200_array);
    			  y_array=&(y200_array);
              break;
    default : cout<<endl<<"Code not recognised.";
              sleep(2);
    			  exit(-1);
   }
  cout<<endl<<"The data points are shown below";
  cout<<endl<<" X "<<'\t'<<" Y "<<endl;
  for(int i=0;i<n;i++)
   {
     cout<<x_array[i];
  	  cout<<'\t';
  	  cout<<y_array[i];
  	  cout<<endl;
   }
  here:cout<<endl<<"Please enter the value of x for which y is to"
  			  <<endl<<"be found out:";
  		 cin>>input_x;
  extrapolated_y=interpolate(x_array,y_array,n,input_x);
  cout<<endl<<"The extrapolated value of y is "<<extrapolated_y;
  cout<<endl<<"Do you wish to continue(y/n)? ";
  cin>>ans;
  if(ans=='y' || ans=='Y')
   {
    cout<<endl<<"Repeating procedure.....(please wait)";
    sleep(3);
    goto here;
   }
  cout<<endl<<"\n Press any key to stop program.";
  getch();
 }
